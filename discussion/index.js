/*   s28-index.js    */

// ****** JS SYNCHRONOUS vs JS ASYNCHRONOUS ******

// JS Synchronous programming - only 1 stmt/line of codes is being processed at a time being used 
	//by JS by default

// the error checking proves the synchronous programming of javascript since after detecting 
	//the error, the next lines of codes will not be executed even if they have no errors


console.log("Hello World");
console.log("Hello Again");
console.log("Goodbye");
// ***** output *******
// Hello World
// Hello Again
// Goodbye
// ====================


console.log("Hello World");
//conosole.log("Hello Again");
console.log("Goodbye");
// ***** output *******
// Hello World
// index.js:21 Uncaught ReferenceError: conosole is not defined
//    at index.js:21:1
// ====================


// when certain stmts take a lot of time to process, this slows down the running/executing of 
		//codes
// an example of this is when loops are used on a large amt of info or when fetching data fr db
// when an action will take some time to be executed, this results in "code blocking"
	// code blocking - delaying of a more efficient code compared to ones currently executed
console.log("Hello World");
// we might not notice it due to improved processing power of our devices, but the process of 
	//fetching/using large amts of info in our loops takes too much compared to logging "Hello 
	//Again"
	// another example is when you try to accesss a website & it takes a while to load(while
	// webpage is being displayed before the landing page is loaded)


for (let i = 0; i <= 1500; i++){
	console.log(i);
};
console.log("Hello Again");  // takes less time to load than the loop


// ***** output *******
// Hello World
// Hello Again
// Goodbye
// Hello World
//  0
//  1
//  2
//	.
//	.
//	.
// 1499
// 1500
// Hello Again
// ====================

// *******************************************************
// *******************************************************

/*
	JAVASCRIPT ASYNCHRONOUS CODE
// Asynchronous JS
// Asynchronous - executing at the same time
	//most asynch js operations can be classified through:
		// Browser/WEB API events or functions; include methods like setTimeout, or event 
			//handlers like onclick, mouse over scroll, etc/; thus, asynchronous means that we 
			//can proceed to execute other statements, while time consuming code is running in 
			//the background
		// it prioritizes the most efficient codes to run while also performing the less 
			//efficient ones in the background

*/


// FETCH function Fetch aPI - allows us to asynchronously fetch/rqst for a resource (data)
// a "promise" is an obj that represents eventual completion (of failure) of an asynchronous 
// function & its resulting
// SYNTAX:
/*
	fetch('URL')
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	SYNTAX: //PREFERRED: more efficient & readable
		fetch(url).then((parameter) => statement).then((parameter) => statement)
*/

// retrieves all posts ff the REST API method (read/GET)
// by using the .then method, we can now check the status of the promise 
fetch("https://jsonplaceholder.typicode.com/posts")
// since fetch method returns a "promise", the "then" method will catch the promise & make it
// the "response" obj
// also, the then method captures the "response" obj & returns another promise w/c will 
// eventually be rejected/resolved
.then(response => console.log(response.status)); //response(dev-defined)-->fr prev function fetch
// this will be logged first, before the status of the promise due to js asynchronous
console.log("Hello Again");
// **** output ******
// hello world
// script.js:15 Hello Again
// script.js:13 200



fetch("https://jsonplaceholder.typicode.com/posts")
// the use of "json()" is to convert the response obj into json format to be used by the 
	//applic
.then((response) => response.json())
// since we cannot directly print the json format of the response in the .then method on the 2nd
	 // line, we need another .then method to catch the promise & print the "response.json()" w/c 
	 //is being represented by the "json" parameter
// using then methods multiple times would create promise chains
.then((json) => console.log(json));  // json(yellow) dev-defined
// **** output ******
//(100) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…},
// {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, 
// {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, 
// {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, 
// {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, 
// {…}, {…}, {…}, {…}, {…}, {…}, {…}]


// ASYNC-AWAIT
// async-await keywords is another approach that can be used to achieve js asynchronous
// used in functions to indicate w/c portions of code shd be waited 
// the codes outside the functions will be executed under JS asynchronous
async function fetchData() {
	// waits for the fetch method to be done before storing value of the response in the "result" 
		//variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	// returns a promise
	console.log(result);
	// returns the type of data the "result" variable has
	console.log(typeof result);
	// we cannot access the body of the result
	console.log(result.body);
	// converts the data fr the "result" variable into json & stores it in "json" variable
	let json = await result.json();
	console.log(json);
}
fetchData();
console.log("Hello Again");
// ***** output *******
// Line 155 ==>
// Hello Again
// Line 145 ==>
// Response {type: 'cors', url: 'https://jsonplaceholder.typicode.com/posts', redirected: false, status: 200, ok: true, …}
// body: (...)
// bodyUsed: true
// headers: Headers {}
// ok: true
// redirected: false
// status: 200
// statusText: ""
// type: "cors"
// url: "https://jsonplaceholder.typicode.com/posts"
// [[Prototype]]: Response
// Line 147 ==>
// object
// Line 149 ==>
// Response {type: 'cors', url: 'https://jsonplaceholder.typicode.com/posts', redirected: false, status: 200, ok: true, …}
// body: (...)
// bodyUsed: true
// headers: Headers {}
// ok: true
// redirected: false
// status: 200
// statusText: ""
// type: "cors"
// url: "https://jsonplaceholder.typicode.com/posts"
// [[Prototype]]: Response
// Line 152
// (100) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]



/*
	mini activity
	using the then method, retrieve the 1st obj in the jsonplaceholder url, 
		the response shd be converted 1st into json format before being displayed in the 
		console
*/

//retrieves a specific obj using the id(landingUrl.com/posts/:id, GET method)
//fetch("https://jsonplaceholder.typicode.com/posts")
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((json) => console.log(json[0]));  
// ***** output *******
// Object
// body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
// id: 1
// title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
// userId: 1
// [[Prototype]]: Object


//CREATE/UPDATE A RESOURCE
/*
	SYNTAX: ( more efficient )
	fetch("url", {options}, details of the rqst body)
	.then(response => {})
	.then(json) => {})
*/

// using post method to create obj (posts/:id, POST)

fetch("https://jsonplaceholder.typicode.com/posts",{
	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: "POST",
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		"Content-Type": "application/json"
	},
	// Sets the content/body data of the "Request" object 
	// JSON.stringify converts the object data into a stringified JSON
	body:JSON.stringify({
		userId: 1,
		title: "New Post",
		body:"Hello World"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));
// ***** output *******
//Object
// body: "Hello World"
// id: 101
// title: "New Post"
// userId: 1[[Prototype]]: Object



// UPDATING A RESOURCE

/*
	PUT - replaces the whole object
	PATCH - updates the specified key/s

*/

/*
	mini activity
	create another fetch rqst (the url shd contain 1 as the id endpt) w/ PUT method
		just only specify the title
			title: corrected post
*/



//.......CONT  line 139 ............
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
		body:JSON.stringify({    // data stored in local storage only
		
			title: "Corrected Post"
		
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// ***** output *******  // PUT replaces the whole obj (entire property)
//Object
// id: 1
// title: "Corrected Post"
// [[Prototype]]: Object

// Postman
// s28
// get rqst  --- body not needed
// https://jsonplaceholder.typicode.com/posts
// post rqst --- raw, json(body); when data is needed to be inserted
// {
// 	"name": "Bogart"
// }



// Updating a Resource

/*
	PUT - replaces the whole obj
	PATCH - updates the specified key/s
*/

/*
	mini activity
	create another fetch rqst (the url shd contain 1 as the id endpoint) w/ PATCH method
		userId: 1
		title: Updated Post
*/

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
		body:JSON.stringify({    // data stored in local storage only
		
			title: "Updated Post",
			userId: 1
		
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// ***** output *******  // PATCH -- only replaces specified key: title & userId in this case
// Object
// body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit 
	//molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
// id: 1
// title: "Updated Post"
// userId: 1
// [[Prototype]]: Object


// deleting a resource   (.then not needed since there is nothing to convert)
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE"
})

// POSTMAN
// DELETE (!!!Careful)
// https://jsonplaceholder.typicode.com/posts/1    (only deleting the 1st entry)
// 
// {}   (returned a form of an empty obj showing that it has been deleted theoretically, still 
	// inside the db(archived); 



/*
	Filtering posts
*/




// filtering .....
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json)); 
// ***** output ******* 
//index.js:283 
// (10) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
// 0: {userId: 1, id: 1, title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit', body: 'quia et suscipit\nsuscipit recusandae consequuntur …strum rerum est autem sunt rem eveniet architecto'}
// 1: {userId: 1, id: 2, title: 'qui est esse', body: 'est rerum tempore vitae\nsequi sint nihil reprehend…aperiam non debitis possimus qui neque nisi nulla'}
// 2: {userId: 1, id: 3, title: 'ea molestias quasi exercitationem repellat qui ipsa sit aut', body: 'et iusto sed quo iure\nvoluptatem occaecati omnis e…\nmolestiae porro eius odio et labore et velit aut'}
// 3: {userId: 1, id: 4, title: 'eum et est occaecati', body: 'ullam et saepe reiciendis voluptatem adipisci\nsit … ipsam iure\nquis sunt voluptatem rerum illo velit'}
// 4: {userId: 1, id: 5, title: 'nesciunt quas odio', body: 'repudiandae veniam quaerat sunt sed\nalias aut fugi…sse voluptatibus quis\nest aut tenetur dolor neque'}
// 5: {userId: 1, id: 6, title: 'dolorem eum magni eos aperiam quia', body: 'ut aspernatur corporis harum nihil quis provident …s\nvoluptate dolores velit et doloremque molestiae'}
// 6: {userId: 1, id: 7, title: 'magnam facilis autem', body: 'dolore placeat quibusdam ea quo vitae\nmagni quis e…t excepturi ut quia\nsunt ut sequi eos ea sed quas'}
// 7: {userId: 1, id: 8, title: 'dolorem dolore est ipsam', body: 'dignissimos aperiam dolorem qui eum\nfacilis quibus…\nipsam ut commodi dolor voluptatum modi aut vitae'}
// 8: {userId: 1, id: 9, title: 'nesciunt iure omnis dolorem tempora et accusantium', body: 'consectetur animi nesciunt iure dolore\nenim quia a…st aut quod aut provident voluptas autem voluptas'}
// 9: {userId: 1, id: 10, title: 'optio molestias id quia eum', body: 'quo et expedita modi cum officia vel magni\ndolorib…it\nquos veniam quod sed accusamus veritatis error'}
// length: 10
// [[Prototype]]: Array(0)

// filtering using multiple parameters
fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=3')
.then((response) => response.json())
.then((json) => console.log(json)); 

// https://jsonplaceholder.typicode.com/posts/1/comments


/*
		mini activity
		retrieve the nested comments array in the 1st entry using the get method
*/

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json)); 
// ***** output ******* 
// /(5) [{…}, {…}, {…}, {…}, {…}]
// 0: {postId: 1, id: 1, name: 'id labore ex et quam laborum', email: 'Eliseo@gardner.biz', body: 'laudantium enim quasi est quidem magnam voluptate …utem quasi\nreiciendis et nam sapiente accusantium'}
// 1: {postId: 1, id: 2, name: 'quo vero reiciendis velit similique earum', email: 'Jayne_Kuhic@sydney.com', body: 'est natus enim nihil est dolore omnis voluptatem n…iatur\nnihil sint nostrum voluptatem reiciendis et'}
// 2: {postId: 1, id: 3, name: 'odio adipisci rerum aut animi', email: 'Nikita@garfield.biz', body: 'quia molestiae reprehenderit quasi aspernatur\naut …mus et vero voluptates excepturi deleniti ratione'}
// 3: {postId: 1, id: 4, name: 'alias odio sit', email: 'Lew@alysha.tv', body: 'non et atque\noccaecati deserunt quas accusantium u…r itaque dolor\net qui rerum deleniti ut occaecati'}
// 4: {postId: 1, id: 5, name: 'vero eaque aliquid doloribus et culpa', email: 'Hayden@althea.biz', body: 'harum non quasi et ratione\ntempore iure ex volupta…ugit inventore cupiditate\nvoluptates magni quo et'}
// length: 5
// [[Prototype]]: Array(0)

fetch("https://jsonplaceholder.typicode.com/posts/?userId=1/comments")
.then((response) => response.json())
.then((json) => console.log(json)); 
// ***** output ******* 
// []
// length: 0
// [[Prototype]]: Array(0)