/* s28-activity script.js */

console.log("hello world");

// JSON Placeholder API:
// https://jsonplaceholder.typicode.com/todos


// =====================================================

// 3. Create a fetch request using the GET method that will retrieve all the to do list items 
	//from JSON Placeholder API.
 

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));  


// =====================================================

// 4. Using the data retrieved, create an array using the map method to return just the title of 
	//every item and print the result in the console. 

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let info = json.map((todo => {
		return todo.title;
	}))

	console.log(info);

});

// =====================================================

// 5. Create a fetch request using the GET method that will retrieve a single to do list item 
	//from JSON Placeholder API. 
// 6. Using the data retrieved, print a message in the console that will provide the title and 
	//status of the to do list item. 

fetch('https://jsonplaceholder.typicode.com/todos/4')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of 
	${json.completed}`)); 

// =====================================================

// 7. Create a fetch request using the POST method that will create a to do list item using the 
	//JSON Placeholder API. 



fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
		body:JSON.stringify({    
		userId: 1,
		title: "New To Do Task",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// =====================================================

// 8. Create a fetch request using the PUT method that will update a to do list item using the 
	//JSON Placeholder API. 
// 9. Update a to do list item by changing the data structure to contain the following 
	//properties: - Title - Description - Status - Date Completed - User ID 



fetch("https://jsonplaceholder.typicode.com/todos/1",{
method: "PUT",
headers: {
		"Content-Type": "application/json"
},
		body:JSON.stringify({    
		
			title: "Update to Last Week's Post",
			description: "Our Palawan Vacay",
			status: "Pending",
			date_completed: "Pending",
			userId: 1	
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// =====================================================

// 10. Create a fetch request using the PATCH method that will update a to do list item using 
	//the JSON Placeholder API. 
// 11. Update a to do list item by changing the status to complete and add a date when the 
	//status was changed. 



fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
		body:JSON.stringify({    
		
			status: "Complete",
			date_changed: "2022-03-15T15:00:00Z"
		
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// =====================================================

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON 
	//Placeholder API. 



fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
})


// =====================================================
// =====================================================


































// ****************************************************************************
// ****************************************************************************

// 13. Create a request via Postman to retrieve all the to do list items. - GET HTTP method - 
	//https://jsonplaceholder.typicode.com/todos URI endpoint - Save this request as get all to 
	//do list items 

// Get Rqst-get all to do list items
// ***** output ******
// [
//     {
//         "userId": 1,
//         "id": 1,
//         "title": "delectus aut autem",
//         "completed": false
//     },
//	.
//	.
//	.
//     {
//         "userId": 10,
//         "id": 200,
//         "title": "ipsam aperiam voluptates qui",
//         "completed": false
//     }
//  ]

// =====================================================

// 14. Create a request via Postman to retrieve an individual to do list item. - GET HTTP method 
	//- https://jsonplaceholder.typicode.com/todos/1 URI endpoint - Save this request as get to 
	//do list item


// Get Rqst-get to do list item
// ***** output ******
// {
//     "userId": 1,
//     "id": 1,
//     "title": "delectus aut autem",
//     "completed": false
// }

// =====================================================

// 15. Create a request via Postman to create a to do list item. - POST HTTP method - 
	//https://jsonplaceholder.typicode.com/todos URI endpoint - Save this request as create to 
	//do list item

// Post Rqst-create to do list item
// body - raw - json
// {
// 	"userId": 1,
// 	"title": "New To Do Task",
// 	"completed": false
// }
// ***** output ******
// {
//     "userId": 1,
//     "title": "New To Do Task",
//     "completed": false,
//     "id": 201
// }

// =====================================================

// 16. Create a request via Postman to update a to do list item. - PUT HTTP method - 
	//https://jsonplaceholder.typicode.com/todos/1 URI endpoint - Save this request as update to 
	//do list item PUT - Update the to do list item to mirror the data structure used in the PUT 
	//fetch request 

// Put Rqst-update to do list item
// body - raw - json
// {
// 	"title": "Update to Last Week's Post",
// 	"description": "Our Palawan Vacay",
// 	"status": "Updated",
// 	"date_completed": "2022-03-15T15:00:00Z",
// 	"userId": 1	
// }

// ***** output ******
// {
//     "title": "Update to Last Week's Post",
//     "description": "Our Palawan Vacay",
//     "status": "Updated",
//     "date_completed": "2022-03-15T15:00:00Z",
//     "userId": 1,
//     "id": 1
// }
// =====================================================

// 17. Create a request via Postman to update a to do list item. - PATCH HTTP method - 
	//https://jsonplaceholder.typicode.com/todos/1 URI endpoint - Save this request as update to 
	//do list item PATCH - Update the to do list item to mirror the data structure of the PATCH 
	//fetch request 

// Patch Rqst-update to do list item
// body - raw - json
// {
// 	"status": "Complete",
// 	"date_changed": "2022-03-15T15:00:00Z"
// }

// ***** output ******
// {
//     "userId": 1,
//     "id": 1,
//     "title": "delectus aut autem",
//     "completed": false,
//     "status": "Complete",
//     "date_changed": "2022-03-15T15:00:00Z"
// }
// =====================================================

// 18. Create a request via Postman to delete a to do list item. - DELETE HTTP method - 
	//https://jsonplaceholder.typicode.com/todos/1 URI endpoint - Save this request as delete to 
	//do list item

// Delete Rqst-delete to do list item

// ***** output ******
//         {}
// =====================================================








